import moment from 'moment';

const applyDelimiter = (value, delimiter) => Array
  .from(String(parseInt(value, 10)))
  .reverse()
  .join('')
  .match(/.{1,3}/g)
  .reverse()
  .map((item) => item.split('').reverse().join(''))
  .join(delimiter);

const returnFormattedFloat = ({
  value: sourceValue,
  decimalDelimiter,
  decimalLength,
  thousandDelimiter,
}) => {
  let value = parseFloat(sourceValue);

  // eslint-disable-next-line no-restricted-globals
  if (!isNaN(value)) {
    const intPart = Math.floor(value);
    const remainPart = (
      Array(decimalLength).fill('0').join('')
      + String(Math.round((value % 1) * (10 ** decimalLength)))
    ).slice(-1 * decimalLength);

    value = applyDelimiter(intPart, thousandDelimiter)
      + (decimalLength > 0 ? (
        decimalDelimiter
        + applyDelimiter(remainPart, thousandDelimiter)
      ) : '');
  } else {
    value = sourceValue;
  }

  return value;
};

export default ({
  records,
  columns,
}) => {
  const response = [];

  records.forEach((row) => {
    const record = {};
    columns.forEach(({ field, type, config }, index) => {
      let value;

      if (!(field in row)) {
        return null;
      }

      if (type === 'text') {
        const {
          maxLength,
        } = config;

        value = String(row[field]);

        if (maxLength > 0) {
          value = value.substring(0, maxLength);

          if (String(row[field]).length > maxLength) {
            value += '...';
          }
        }
      } else if (type === 'number') {
        const {
          type: fieldType,
          decimalDelimiter,
          decimalLength,
          thousandDelimiter,
        } = config;

        if (fieldType === 'float') {
          value = returnFormattedFloat({
            value: String(row[field]),
            decimalLength,
            decimalDelimiter,
            thousandDelimiter,
          });
        } else {
          value = parseInt(row[field], 10);

          // eslint-disable-next-line no-restricted-globals
          if (!isNaN(value)) {
            value = applyDelimiter(value, thousandDelimiter);
          } else {
            value = String(row[field]);
          }
        }
      } else if (type === 'datetime') {
        const date = moment(row[field], config.format);

        const {
          format,
        } = config;

        value = date.format(format);
      } else if (type === 'boolean') {
        const {
          useCheckbox,
          trueLabel,
          falseLabel,
        } = config;

        // eslint-disable-next-line no-nested-ternary
        value = row[field] ? (useCheckbox ? '✔️' : trueLabel) : (useCheckbox ? '❌' : falseLabel);
      } else if (type === 'json') {
        if (config.pretty) {
          value = JSON.stringify(JSON.parse(String(row[field])), null, 2);
        } else {
          value = String(row[field]);
        }
      } else {
        value = String(row[field]);
      }

      record[`field_${ index }`] = value;
    });
    response.push(record);
  });

  return response;
};
