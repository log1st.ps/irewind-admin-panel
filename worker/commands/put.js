import axios from 'axios';
import qs from 'qs';
import { buildAxiosConfig, getApiUrl } from '../common';

export default (
  {
    url,
    config,
    data,
  },
  token,
) => new Promise((resolve, reject) => {
  axios.put(
    getApiUrl(url),
    qs.stringify(data),
    buildAxiosConfig({
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      ...config,
    }, token),
  )
    .then(({ data: response }) => {
      resolve(response);
    })
    .catch((res) => {
      reject(res);
    });
});
