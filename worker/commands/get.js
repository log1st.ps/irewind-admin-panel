import axios from 'axios';
import { buildAxiosConfig, getApiUrl } from '../common';

export default (
  {
    url,
    config,
  },
  token,
) => new Promise((resolve, reject) => {
  axios.get(
    getApiUrl(url),
    buildAxiosConfig(config, token, !Array.isArray(url)),
  )
    .then(({ data, status }) => {
      if (status === 400) {
        console.log('Retrying request because of 400 error...');
        axios.get(
          getApiUrl(url),
          buildAxiosConfig(config, token, !Array.isArray(url)),
        )
          .then(({ data: newData }) => {
            resolve(newData);
          })
          .catch((newData) => {
            reject(newData);
          });
      } else {
        resolve(data);
      }
    })
    .catch((data) => {
      reject(data);
    });
});
