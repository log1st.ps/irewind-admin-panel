import axios from 'axios';
import { buildAxiosConfig, getApiUrl } from '../common';

export default (
  {
    url,
    data,
    config,
  },
  token,
) => new Promise((resolve, reject) => {
  axios.delete(
    getApiUrl(url) + (data
      ? (`?${ Object.entries(data).map(([key, value]) => `${ key }=${ value }`).join('&') }`)
      : null),
    buildAxiosConfig(config, token),
  )
    .then(({ data: response }) => {
      resolve(response);
    })
    .catch((res) => {
      reject(res);
    });
});
