export default ({
  users,
  columns,
  attachedColumns,
}) => users.map((user) => {
  const reversedAttachedColumns = {};
  Object.entries(attachedColumns).forEach(([key, value]) => {
    reversedAttachedColumns[value] = key;
  });

  const computedUser = {};
  columns.forEach(({ field }) => {
    computedUser[field] = user[reversedAttachedColumns[field]];
  });

  return computedUser;
});
