import moment from 'moment';
import { dateToMomentFormatter } from '../../src/utilities/dateParseRegex';

export default ({
  users,
  attachedColumns,
  timestampColumns,
  columnCameras,
  timeZone,
  date,
  isTimeZoneDetected,
  filters,
}) => {
  const reversedAttachedColumns = {};
  Object.entries(attachedColumns).forEach(([key, value]) => {
    reversedAttachedColumns[value] = key;
  });
  const timings = [];

  const parsedDate = moment(date, 'YYYY-MM-DD');
  const parsedTimeZone = moment(timeZone, 'HH:mm');

  const timestampFormatter = (type, value) => (({
    timestamp() {
      return (+value) * 1000;
    },
    timestampWithMilliseconds() {
      return +value;
    },
    date: () => {
      const parsed = dateToMomentFormatter.date(value);

      return +parsed;
    },
    fullDate() {
      const parsed = dateToMomentFormatter.fullDate(value);

      return +parsed;
    },
    datetime() {
      const parsed = dateToMomentFormatter.datetime(value);

      return +parsed;
    },
    time: () => {
      const parsed = dateToMomentFormatter.time(parsedDate, value);

      return +parsed;
    },
  })[type] || (() => null))();

  const computedTimestampColumns = [];

  Object.entries(timestampColumns)
    .forEach(([key, [isTimestamp, type]]) => {
      if (isTimestamp) {
        computedTimestampColumns.push([key, type]);
      }
    });

  users
    .forEach((user, i) => {
      computedTimestampColumns
        .forEach(([key, type]) => {
          columnCameras[key].forEach((camera, index) => {
            timings.push({
              race_id: user[reversedAttachedColumns.race_id],
              concurent_no: user[reversedAttachedColumns.concurent_no],
              camera,
              timestamp:
                timestampFormatter(type, user[key])
                + (
                    isTimeZoneDetected
                      ? 0
                      : -((+parsedTimeZone.format('H') * 60 * 60 + (+parsedTimeZone.format('m')) * 60) * 1000)
                ) + index * 50,
            });
          });
        });
    });

  const filteredTimings = timings.filter((timing) => {
    let valid = true;

    Object.entries(filters).forEach(([key, value]) => {
      if (String(value).length && valid && !String(timing[key]).match(value)) {
        valid = false;
      }
    });

    return valid;
  });

  return filteredTimings;
};
