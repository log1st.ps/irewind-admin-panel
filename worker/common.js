// eslint-disable-next-line no-undef
export const getApiUrl = (endpoint) => (Array.isArray(endpoint) ? endpoint[0] : `${ API_URL }${ endpoint }`);

export const buildAxiosConfig = (config, token = null, withToken = true) => ({
  ...(config || {}),
  headers: {
    ...((token && withToken) ? {
      Authorization: `Bearer ${ token }`,
    } : {}),
    ...(config ? config.headers : {}),
  },
  validateStatus: false,
});
