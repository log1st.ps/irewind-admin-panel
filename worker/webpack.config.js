const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  context: path.resolve('./worker'),
  entry: {
    worker: './worker.js',
  },
  output: {
    path: path.resolve('./src/statics/'),
    filename: '[name].js',
    library: '[name]',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [
          /node_modules/,
        ],
        options: {
          presets: [
            '@quasar/babel-preset-app',
          ],
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(process.env.API_URL || 'https://vps.irewind.com/video-processor-secured/services/'),
    }),
  ],
};
