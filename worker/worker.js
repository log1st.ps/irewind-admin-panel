import get from './commands/get';
import post from './commands/post';
import put from './commands/put';
import deleteRequest from './commands/delete';
import formatDataResponse from './commands/formatDataResponse';
import computeUsersInTiming from './commands/computeUsersInTiming';
import computeTimingsInTiming from './commands/computeTimingsInTiming';

self.addEventListener(
  'message',
  ({ data: { command, params, token }, ports }) => {
    if (command === 'get') {
      get(params, token)
        .then((res) => {
          ports[0].postMessage(res);
        })
        .catch((res) => {
          ports[0].postMessage({
            error: res.message,
          });
        });
    } else if (command === 'post') {
      post(params, token)
        .then((res) => {
          ports[0].postMessage(res);
        })
        .catch((res) => {
          console.log('Error while making post-request', res);
        });
    } else if (command === 'put') {
      put(params, token)
        .then((res) => {
          ports[0].postMessage(res);
        })
        .catch((res) => {
          console.log('Error while making put-request', res);
        });
    } else if (command === 'delete') {
      deleteRequest(params, token)
        .then((res) => {
          ports[0].postMessage(res);
        })
        .catch((res) => {
          console.log('Error while making put-request', res);
        });
    } else if (command === 'formatDataResponse') {
      ports[0].postMessage(
        formatDataResponse(params),
      );
    } else if (command === 'computeUsersInTiming') {
      ports[0].postMessage(
        computeUsersInTiming(params),
      );
    } else if (command === 'computeTimingsInTiming') {
      ports[0].postMessage(
        computeTimingsInTiming(params),
      );
    }
  },
);
