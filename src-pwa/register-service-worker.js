/*
 * This file is picked up by the build system only
 * when building for PRODUCTION
 */

import { register } from 'register-service-worker';

if (process.env.ENV_NODE === 'production') {
  register(process.env.SERVICE_WORKER_FILE, {
    ready() {
      // eslint-disable-next-line
      console.log('App is being served from cache by a service worker.');
    },
    cached() {
      // eslint-disable-next-line
      console.log('Content has been cached for offline use.');
    },
    updated() {
      // eslint-disable-next-line
      console.log('New content is available; please refresh.');
    },
    offline() {
      // eslint-disable-next-line
      console.log('No internet connection found. App is running in offline mode.');
    },
    error(err) {
      // eslint-disable-next-line
      console.error('Error during service worker registration:', err);
    },
  });
}
