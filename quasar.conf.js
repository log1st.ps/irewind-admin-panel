// Configuration for your app

const DefinePlugin = require('webpack').DefinePlugin;

module.exports = function conf(ctx) { // eslint-disable-line no-unused-vars
  return {
    plugins: [
      'croppa',
      'http',
      'google-maps',
      'vuelidate',
      'vuex-router-sync',
      'api-worker',
      'draggable',
      'eventHub',
    ],
    css: [
      'app.styl',
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons',
      // 'ionicons',
      // 'mdi',
      'fontawesome',
    ],
    supportIE: true,
    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      publicPath: '/admin/',
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      // useNotifier: false,
      extendWebpack(cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/,
        });

        cfg.plugins.push(new DefinePlugin({
          API_URL: JSON.stringify(process.env.API_URL || 'https://vps.irewind.com/video-processor-secured/services/'),
        }));
      },
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true, // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      components: [
        'QBtn',
        'QBtnGroup',
        'QBtnDropdown',
        'QCard',
        'QCardActions',
        'QCardMain',
        'QCardMedia',
        'QCardSeparator',
        'QCardTitle',
        'QCheckbox',
        'QChipsInput',
        'QChip',
        'QAutocomplete',
        'QCollapsible',
        'QColor',
        'QDatetime',
        'QDialog',
        'QField',
        'QIcon',
        'QInnerLoading',
        'QInput',
        'QInputFrame',
        'QItem',
        'QItemSeparator',
        'QPageSticky',
        'QItemMain',
        'QItemSide',
        'QItemTile',
        'QLayout',
        'QLayoutDrawer',
        'QLayoutFooter',
        'QLayoutHeader',
        'QList',
        'QListHeader',
        'QModal',
        'QPage',
        'QPageContainer',
        'QPagination',
        'QPopover',
        'QProgress',
        'QRadio',
        'QRouteTab',
        'QScrollArea',
        'QSearch',
        'QSelect',
        'QSlider',
        'QSpinnerGears',
        'QTable',
        'QTabs',
        'QTab',
        'QTabPane',
        'QTd',
        'QTh',
        'QTr',
        'QToggle',
        'QToolbar',
        'QToolbarTitle',
        'QSpinner',
      ],
      directives: [
        'Ripple',
        'TouchPan',
        'CloseOverlay',
      ],
      plugins: [
        'Dialog',
        'LocalStorage',
        'Notify',
        'Platform',
      ],
    },
    // animations: 'all' --- includes all animations
    animations: [
      'bounceInLeft',
      'bounceOutRight',
    ],
    pwa: {
      manifest: {
        // name: 'iRewind Admin',
        // short_name: 'iRewind Admin',
        // description: 'Best PWA App in town!',
        icons: [
          {
            src: 'statics/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png',
          },
          {
            src: 'statics/icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        permissions: ['fileBrowserHandler', 'fileSystem', 'mediaGalleries', 'storage', 'system.storage', 'videoCapture'],
      },
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      extendWebpack(cfg) { // eslint-disable-line no-unused-vars
        // do something with cfg
      },
      packager: {
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
    },

    // leave this here for Quasar CLI
    starterKit: '1.0.0-beta.4',
  };
};
