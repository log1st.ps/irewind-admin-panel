module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
  // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
  extends: ['plugin:vue/essential', 'airbnb-base'],
  // required to lint *.vue files
  plugins: [
    'vue',
  ],
  globals: {
    'ga': true, // Google Analytics
    'cordova': true,
    '__statics': true,
  },
  // add your custom rules here
  rules: {
    'no-param-reassign': 0,

    'import/first': 0,
    'import/named': 2,
    'import/namespace': 2,
    'import/default': 2,
    'import/export': 2,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-dynamic-require': 0,
    'import/prefer-default-export': 0,

    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,

    'linebreak-style': 'off',
    'arrow-parens': ['error', 'always'],
    'max-len': ['error', 200],
    'template-curly-spacing': ['error', 'always'],
    'no-underscore-dangle': 'off',
    'no-var': 'error',
    'no-param-reassign': ['error', { 'props': false }],
    indent: 'off',
    'indent-legacy': ['error', 2, {
      SwitchCase: 1,
    }],
    vue: {
      'script-indent': ['error', 2, {
        'baseIndent': 1,
        'switchCase': 1,
        'ignores': [],
      }],
    },
  },
};
