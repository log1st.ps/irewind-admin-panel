import http from 'axios';
import { LocalStorage } from 'quasar';
import { apiPublicUrl } from '../../config';

const store = {
  namespaced: true,
  state: {
    required: false,
    credentials: { },
    message: null,
    cancellable: false,
  },
  getters: {
    needAuth: (state) => (
      (typeof state.message === 'string' && state.message.length > 0) || (!state.credentials.token && state.required)
    ),
  },
  actions: {
    wait({ commit, dispatch, state }, { message, cancellable }) {
      let done = false;
      let handlerPopstate;
      let handlerAuth;
      let handlerAuthId;
      let calculatedMessage = message;
      const calculatedCancellable = [null, undefined].indexOf(cancellable) === -1 ? !!cancellable : false;
      const { token } = state.credentials;
      if (calculatedMessage === true || token === undefined) {
        calculatedMessage = 'Please authenticate.';
      } else if (typeof calculatedMessage === 'string' && calculatedMessage.length) {
        if (['Invalid token', 'Invalid token format', 'Invalid credentials'].indexOf(calculatedMessage) !== -1) {
          calculatedMessage = 'Authentication expired. Please authenticate again.';
        }
      } else {
        calculatedMessage = null;
      }

      commit('SET_MESSAGE', calculatedMessage);
      commit('SET_CANCELLABLE', calculatedCancellable);
      if (!calculatedCancellable) {
        commit('SET_CREDENTIALS', {
          username: state.credentials.username,
          password: state.credentials.password,
        });
        dispatch('login', state.credentials);
      }
      const promiseFinally = () => {
        done = true;
        clearTimeout(handlerAuthId);
        handlerAuthId = null;
        window.removeEventListener('popstate', handlerPopstate);
        commit('SET_MESSAGE', null);
      };
      return new Promise((resolve, reject) => {
        handlerAuth = () => {
          clearTimeout(handlerAuthId);
          handlerAuthId = null;
          if (!done) {
            if (typeof state.message !== 'string') {
              if (state.credentials.token && token !== state.credentials.token) {
                resolve(http.defaults.headers.common.Authorization);
              } else {
                reject(new Error('Not authenticated'));
              }
            } else {
              handlerAuthId = setTimeout(handlerAuth, 1000);
            }
          }
        };
        handlerAuth();
        handlerPopstate = () => reject(new Error('Cancelled'));
        window.addEventListener('popstate', handlerPopstate);
      })
        .catch((reason) => {
          if (cancellable) {
            return undefined;
          }
          return Promise.reject(reason);
        })
        .then((value) => {
          promiseFinally();
          return value;
        })
        .catch((error) => {
          promiseFinally();
          throw error;
        });
    },
    getInfo(_, token) {
      if (token) {
        http.defaults.headers.common.Authorization = `Bearer ${ token }`;
        return http
          .get(`${ apiPublicUrl }/account/info`)
          .then((response) => response.data);
      }
      return Promise.reject(new Error('Token not present'));
    },
    login({ commit, dispatch }, payload) {
      const credentials = payload || {};
      return (!credentials.provider && credentials.token ? dispatch('getInfo', credentials.token) : Promise.reject(new Error('Not authenticated')))
        .catch(() => {
          let authPayload = {};
          if (credentials.provider) {
            authPayload = {
              provider: credentials.provider,
              token: credentials.token,
            };
            delete credentials.provider;
          } else {
            if (
              typeof credentials.username !== 'string' || !credentials.username.length
              || typeof credentials.password !== 'string' || !credentials.password.length
            ) {
              return Promise.reject(new Error('Not a valid token'));
            }
            authPayload = {
              email: credentials.username,
              password: credentials.password,
              remember_me: false,
            };
          }
          return http
            .post(`${ apiPublicUrl }/account/login`, authPayload)
            .then(({ data }) => {
              const token = data.id_token;
              credentials.token = token;
              return dispatch('getInfo', token);
            });
        })
        .then((info) => {
          commit('SET_MESSAGE', null);
          commit('SET_CANCELLABLE', false);
          commit('SET_CREDENTIALS', { ...credentials, info });
          return info;
        })
        .catch((rejection) => {
          commit('SET_CREDENTIALS', {
            username: credentials.username,
          });
          return Promise.reject(rejection);
        });
    },
    cancel({ commit }) {
      commit('SET_MESSAGE', null);
      commit('SET_CANCELLABLE', false);
    },
    clear({ commit, dispatch }) {
      commit('CLEAR_CREDENTIALS');
      dispatch('logout');
    },
    logout({ commit, state }) {
      commit('SET_MESSAGE', null);
      commit('SET_CANCELLABLE', false);
      commit('SET_CREDENTIALS', {
        username: state.credentials.username,
      });
      delete http.defaults.headers.common.Authorization;
    },
  },
  mutations: {
    SET_REQUIRED(state, payload) {
      state.required = Boolean(payload);
    },
    SET_MESSAGE(state, payload) {
      state.message = payload;
    },
    SET_CANCELLABLE(state, payload) {
      state.cancellable = Boolean(payload);
    },
    SET_CREDENTIALS(state, payload) {
      state.credentials = {
        info: {},
        ...payload,
      };
      LocalStorage.set('credentials', JSON.stringify({
        username: state.credentials.username,
        password: state.credentials.password,
        token: state.credentials.token,
      }));
    },
    CLEAR_CREDENTIALS(state) {
      state.credentials = { info: {} };
      LocalStorage.clear('credentials');
    },
  },
};

export default store;
