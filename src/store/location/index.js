import http, { isCancel } from 'axios';
// eslint-disable-next-line
import Router from '../../router';
import { showRejectionMessage } from '../../utils';

let locationLoadPromise = null;

const store = {
  namespaced: true,
  state: {
    locations: [],
    location: null,
  },
  actions: {
    load({ commit, dispatch, rootState }) {
      if (locationLoadPromise) {
        return locationLoadPromise;
      }
      locationLoadPromise = http
        .get('/locations/list')
        .then((response) => {
          const locations = response.data.results;
          commit('SET_LOCATIONS', locations);
          if (rootState.route.params && typeof rootState.route.params.idLocation === 'string' && rootState.route.params.idLocation.length > 0) {
            dispatch('set', locations.find((l) => String(l.id) === rootState.route.params.idLocation));
          } else {
            dispatch('clear');
          }
          locationLoadPromise = null;
          return locations;
        })
        .catch((rejection) => {
          if (!isCancel(rejection)) {
            showRejectionMessage(rejection, 'There was an error reading locations.');
          }
          commit('CLEAR_LOCATIONS');
          dispatch('clear');
          locationLoadPromise = null;
          return Promise.reject(rejection);
        });
      return locationLoadPromise;
    },
    set({ commit, dispatch, rootState }, payload) {
      if (!payload || !payload.id) {
        dispatch('clear');
      } else {
        commit('SET_LOCATION', payload);
        const routeName = rootState.route.name;
        if (routeName === 'home') {
          Router.push({ name: 'overview', params: { idLocation: payload.id } });
        } else {
          Router.push({ name: routeName, params: { idLocation: payload.id } });
        }
      }
    },
    clear({ commit }) {
      commit('CLEAR_LOCATION');
      Router.push({ name: 'home' });
    },
  },
  mutations: {
    SET_LOCATIONS(state, payload) {
      state.locations = payload;
    },
    CLEAR_LOCATIONS(state) {
      state.locations = [];
    },
    SET_LOCATION(state, payload) {
      state.location = payload;
    },
    CLEAR_LOCATION(state) {
      state.location = null;
    },
  },
};

export default store;
