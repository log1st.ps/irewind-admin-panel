export const redactRecord = (state, {
  record,
  config,
  isNewRecord,
}) => {
  const filledRecord = { ...record };

  config.editors.forEach(({ fields }) => {
    fields.forEach(({ type, config: { field, ...rest } }) => {
      if (field && !(field in filledRecord)) {
        filledRecord[field] = ({
          checkbox: false,
          text: null,
          number: null,
          dropdown: rest.isMultiple ? [] : null,
          collection: rest.isMultiple ? [] : null,
          [null]: null,
          [undefined]: null,
        }[type]);
      }
    });
  });

  state.redactors.push({
    record: { ...filledRecord },
    config,
    isNewRecord,
  });
};

export const unredactRecord = (state, index) => {
  state.redactors.splice(index);
};
