import Vue from 'vue';
import Vuex from 'vuex';

import auth from './auth';
// eslint-disable-next-line
import location from './location';

import config from './config';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    location,
    config,
  },
});

export default store;
