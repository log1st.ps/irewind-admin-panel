import Draggable from 'vuedraggable';

export default ({ Vue }) => {
  Vue.component('draggable', Draggable);
};
