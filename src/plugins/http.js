import http, { isCancel } from 'axios';
import { Notify } from 'quasar';
import store from '../store';
import { apiBaseUrl } from '../config';

const retrySchedule = [1000, 2000, 4000, 6000];
const retryScheduleLength = retrySchedule.length;

http.defaults.baseURL = apiBaseUrl;
http.defaults.timeout = 0;
http.defaults.headers.common.Authorization = null;
http.defaults.headers.post['Content-Type'] = 'application/json';
http.interceptors.response.use(
  (response) => response,
  (error) => {
    const originalRequest = error ? error.config : null;
    if (originalRequest && !isCancel(error)) {
      if (!originalRequest.noRetry && (!error || !error.response || [429, 503].includes(error.response.status))) {
        originalRequest.retryCount = (originalRequest.retryCount || 0) + 1;
        const lastRetry = originalRequest.retryCount >= retryScheduleLength;
        const defaultRetryAfter = lastRetry ? retrySchedule[retryScheduleLength - 1] : retrySchedule[originalRequest.retryCount - 1];
        const retryAfter = error.response ? error.response.headers['Retry-After'] || defaultRetryAfter : defaultRetryAfter;
        return new Promise((resolve, reject) => {
          let cancel = lastRetry;
          Notify.create({
            type: 'negative',
            message: lastRetry ? 'Connection problem' : `Connection problem - re-trying in ${ retryAfter / 1000 } seconds`,
            timeout: retryAfter,
            position: 'bottom-right',
            actions: [{
              label: lastRetry ? 'Retry' : 'Cancel',
              handler: () => {
                cancel = !cancel;
              },
            }],
            onDismiss: () => setTimeout(() => {
              if (cancel) {
                return reject(error || 'Cannot connect to server');
              }
              return http(originalRequest).then(resolve, reject);
            }),
          });
        });
      }
      if (error && error.response && error.response.status === 401 && !originalRequest.noAuthRetry) {
        return store
          .dispatch('auth/wait', {
            message: error.response.data.message === 'Invalid token format' ? true : error.response.data.message || error.response.data.error,
            cancellable: false,
          })
          .catch(() => Promise.reject(error))
          .then((authorization) => {
            originalRequest.headers.Authorization = authorization;
            return http(originalRequest);
          });
      }
    }

    return Promise.reject(error);
  },
);

export default ({ Vue }) => {
  Vue.prototype.$http = http;
};
