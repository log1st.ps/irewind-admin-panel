import loadGoogleMapsAPI from 'load-google-maps-api';
import { Notify } from 'quasar';
import { googleMapsId } from '../config';

export default ({ Vue }) => {
  loadGoogleMapsAPI({
    key: googleMapsId,
    libraries: [
      'places',
    ],
  })
    .then((googleMaps) => {
      Vue.prototype.$maps = googleMaps;
    })
    .catch((err) => {
      Notify.create({
        type: 'negative',
        message: String(err),
        position: 'bottom-right',
      });
    });
};
