import { Notify } from 'quasar';
import { isCancel } from 'axios';

export function showRejectionMessage(rejection, fallback = 'An unknown error occured', type = 'negative') {
  if (isCancel(rejection)) {
    return true;
  }
  if (typeof rejection === 'string') {
    return Notify.create({
      type,
      message: rejection || fallback,
      position: 'bottom-right',
    });
  }
  if (rejection.response) {
    if (rejection.response.data) {
      return Notify.create({
        type,
        message: rejection.response.data.Message || rejection.response.data.message || rejection.response.data.error || fallback,
        position: 'bottom-right',
      });
    }
    return Notify.create({
      type,
      message: rejection.response.statusText || rejection.response.status || fallback,
      position: 'bottom-right',
    });
  }
  return Notify.create({
    type,
    message: rejection.message || fallback,
    position: 'bottom-right',
  });
}

export function getRejectionMessage(rejection, fallback = 'An unknown error occured') {
  if (isCancel(rejection)) {
    return 'Request cancelled';
  }
  if (typeof rejection === 'string') {
    return rejection;
  }
  if (rejection.response) {
    if (rejection.response.data) {
      return rejection.response.data.Message || rejection.response.data.message || rejection.response.data.error || fallback;
    }
    return rejection.response.statusText || rejection.response.status || fallback;
  }
  return rejection.message || fallback;
}

export function padLeft(value, len, char) {
  if ([null, undefined].indexOf(value) !== -1) {
    return value;
  }
  const retVal = String(value);
  const retValLen = retVal.length;
  const lenCalc = parseInt(len || 2, 10);
  const charCalc = [null, undefined, ''].indexOf(char) !== -1 ? '0' : String(char);
  if (retValLen >= lenCalc) {
    return retVal;
  }
  return `${ charCalc.repeat(lenCalc - retValLen) }${ retVal }`;
}
