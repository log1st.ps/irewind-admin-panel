import moment from 'moment';

export const DATE_REGEX_TIMESTAMP = /^\d{10}$/;
export const DATE_REGEX_TIMESTAMP_WITH_MILLISECONDS = /^\d{13}$/;
export const DATE_REGEX_DATE = /^\d{4}-\d{1,2}-\d{1,2}$/;
export const DATE_REGEX_FULL_DATE = /^(\d{4}-\d{1,2}-\d{1,2})T(\d{1,2})(.|,|:)(\d{1,2})(.|,|:)(\d{1,2})((.|,|:)(\d+)|)([+-](\d{1,2}))(.|,|:)(\d{1,2})$/;
export const DATE_REGEX_DATETIME = /^(\d{4}-\d{1,2}-\d{1,2})(\s)(\d{1,2})(.|,|:)(\d{1,2})(.|,|:)(\d{1,2})((.|,|:)(\d+)|)$/;
export const DATE_REGEX_TIME = /^(\d{1,2})(.|,|:)(\d{1,2})(.|,|:)(\d{1,2})((.|,|:)(\d+)|)$/;

export const dateToMomentFormatter = {
  time: (date, item) => {
    if (!item) {
      return false;
    }

    const match = item.match(DATE_REGEX_TIME);

    if (!match) {
      return false;
    }

    const hours = match[1];
    const minutes = match[3];
    const seconds = match[5];
    const hasMilliseconds = match[6];
    const milliseconds = match[8];

    return moment({
      year: date.format('YYYY'),
      month: date.format('M') - 1,
      day: date.format('D'),
      hour: hours,
      minute: minutes,
      second: seconds,
      millisecond: hasMilliseconds ? milliseconds : null,
    }).add(moment().utcOffset(), 'minutes');
  },
  date: (item) => {
    if (!item) {
      return false;
    }
    const match = item.match(DATE_REGEX_DATE);

    if (!match) {
      return false;
    }

    const date = match[1];
    const hours = match[2];
    const minutes = match[4];
    const seconds = match[6];
    const hasMilliseconds = match[7];
    const milliseconds = match[9];

    const parsedDate = moment(date, 'YYYY-MM-DD');

    return moment({
      year: parsedDate.format('YYYY'),
      month: parsedDate.format('M') - 1,
      day: parsedDate.format('D'),
      hour: hours,
      minute: minutes,
      second: seconds,
      millisecond: hasMilliseconds ? milliseconds : null,
    }).add(moment().utcOffset(), 'minutes');
  },
  datetime: (item) => {
    if (!item) {
      return false;
    }
    const match = item.match(DATE_REGEX_DATETIME);

    if (!match) {
      return false;
    }

    const date = match[1];
    const hours = match[3];
    const minutes = match[5];
    const seconds = match[7];
    const hasMilliseconds = match[8];
    const milliseconds = match[10];

    const parsedDate = moment(date, 'YYYY-MM-DD');

    console.log(parsedDate.format('M'));

    return moment({
      year: parsedDate.format('YYYY'),
      month: parsedDate.format('M') - 1,
      day: parsedDate.format('D'),
      hour: hours,
      minute: minutes,
      second: seconds,
      millisecond: hasMilliseconds ? milliseconds : null,
    }).add(moment().utcOffset(), 'minutes');
  },
  fullDate: (item) => {
    if (!item) {
      return false;
    }
    const match = item.match(DATE_REGEX_FULL_DATE);

    if (!match) {
      return false;
    }

    const date = match[1];
    const hours = match[2];
    const minutes = match[4];
    const seconds = match[6];
    const hasMilliseconds = match[7];
    const milliseconds = match[9];

    const parsedDate = moment(date, 'YYYY-MM-DD');

    return moment({
      year: parsedDate.format('YYYY'),
      month: parsedDate.format('M') - 1,
      day: parsedDate.format('D'),
      hour: hours,
      minute: minutes,
      second: seconds,
      millisecond: hasMilliseconds ? milliseconds : null,
    }).add(-match[10], 'hour').add(-match[13], 'second')
      .add(moment().utcOffset(), 'minutes');
  },
};
