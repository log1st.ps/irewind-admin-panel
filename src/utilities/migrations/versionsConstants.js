export const VERSION_OLD = 1;
export const VERSION_NEW = 2;
export const VERSION_METHODS = 3;

export const VERSION_UP_TO_DATE = VERSION_METHODS;
