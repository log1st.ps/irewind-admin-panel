/* eslint-disable */

import {VERSION_METHODS, VERSION_OLD} from "./versionsConstants";
import commonField from "../defaults/commonField";

export default ((config) => {
  const newConfig = {};

  const {
    collection_id,
    collection_name,
    collection_description,
    collection_readonly,
    collection_methods,
    collection_fields,
    expandable,
    reordering,
  } = config;


  newConfig.name = collection_name;
  newConfig.description = collection_description;
  newConfig.version = VERSION_METHODS;
  newConfig.migratedVersion = VERSION_OLD;

  newConfig.methods = Object.entries(collection_methods).map(([
    name,
    {
      url,
      method,
      action_button,
      confirm,
      params,
      description,
    },
  ]) => {
    const type = (name in collection_fields && collection_fields[name].length) ? 'list' : 'action';

    const config = ({
      action: () => ({
        endpoint: url,
        method,
        buttonText: action_button,
        useConfirmation: !!confirm,
        confirmationText: confirm,
        fields: params.map(item => ({
          label: item.label,
          width: 100,
          type: item.collection ? 'collection' : 'text',
          config: {
            ...(item.collection ? {
              collection: item.collection,
              keyField: item.collection_key,
              labelField: item.collection_label,
              isMultiple: false,
              fieldType: 'dropdown',
            } : commonField('text')),
            field: item.id,
          },
        })),
      }),
      list: () => ({
        endpoint: url,
        isEditable: !collection_readonly,
        isRemovable: !collection_readonly,
        defaultSortColumn: null,
        defaultSortDirection: null,
        editors: [],
        filters: params && params.length ? params.map(param => ({
            label: param.label || param.id.split('_').map(
              item => item[0].toUpperCase() + item.substr(1, item.length - 1)
            ).join(' '),
            width: 100,
            type: !!param.collection ? 'collection' : 'text',
            config: {
              ...(param.collection ? {
                collection: param.collection,
                keyField: param.collection_key,
                labelField: param.collection_label,
                isMultiple: false,
                fieldType: 'dropdown',
              } : commonField('text')),
              field: param.id,
            }
          })
        ) : [],
        columns: collection_fields[name].map(({
          field_collection,
          field_display_list,
          field_type,
          field_label,
          field_id,
          field_align,
          field_list_format,
          field_name,
          field_list_width,
        }) => {

          return {
            field: field_name,
            label: field_label || field_name.split('_').map(
              item => item[0].toUpperCase() + item.substr(1, item.length - 1)
            ).join(' '),
            width: field_list_width || null,
            widthUnit: 'px',
            type: {
              text: 'text',
              integer: 'number',
              numeric: 'number',
              boolean: 'boolean',
              date: 'datetime',
              time: 'datetime',
              datetime: 'datetime',
              timestamp: 'datetime',
              json: 'json',
              pre: 'pre',
              url: 'url',
              collection: 'collection',
            }[field_type],
            config: {

            },
          }
        })
      })
    }[type])();

    return {
      name,
      type,
      description,
      config,
    }
  });

  return newConfig;
});
