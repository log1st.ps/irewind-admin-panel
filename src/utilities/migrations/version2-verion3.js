import { VERSION_METHODS, VERSION_NEW } from './versionsConstants';
import { collectionListTypeConfig, collectionMethod } from '../defaults/collection';

export default ((config) => {
  const newConfig = {
    methods: [],
  };

  const {
    collection_name: collectionName,
    description,
    name,
    endpoint,
    editors,
    columns,
    defaultSortColumn,
    defaultSortDirection,
    isEditable,
    isRemovable,
    actions,
  } = config;

  newConfig.name = collectionName;
  newConfig.description = description;
  newConfig.version = VERSION_METHODS;
  newConfig.migratedVersion = VERSION_NEW;

  newConfig.methods.push({
    ...collectionMethod,
    ...{
      name,
      description,
      type: 'list',
      config: {
        ...collectionListTypeConfig,
        ...{
          endpoint,
          editors,
          columns,
          defaultSortColumn,
          defaultSortDirection,
          isEditable,
          isRemovable,
        },
      },
    },
  });

  if (actions.length) {
    actions.forEach(({
      name: actionName,
      description: actionDescription,
      ...action
    }) => {
      newConfig.methods.push({
        name: actionName,
        description: actionDescription,
        type: 'action',
        config: {
          ...action,
          fields: action.fields.map((item) => ({
            ...item,
            config: {
              field: item.model,
              ...item.config,
            },
          })),
        },
      });
    });
  }

  return newConfig;
});
