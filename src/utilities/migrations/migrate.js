/* eslint-disable */

import defaultCollectionConfig from '../defaults/collection';

import versionOldMigrator from './version1-verion3';
import versionNewMigrator from './version2-verion3';
import { VERSION_NEW, VERSION_OLD, VERSION_UP_TO_DATE } from './versionsConstants';

const migrations = {
  [VERSION_OLD]: versionOldMigrator,
  [VERSION_NEW]: versionNewMigrator,
};


export default (config) => {
  if (JSON.stringify(config) === JSON.stringify({})) {
    return { ...defaultCollectionConfig };
  }

  const {
    __migratedConfig
  } = config

  if(__migratedConfig) {
    return __migratedConfig
  }

  let newConfig = config;

  if((config.version || 1) < VERSION_UP_TO_DATE) {
    do {
      newConfig = migrations[newConfig.version || 1](newConfig);
    } while ((newConfig.version || 1) < VERSION_UP_TO_DATE);
  }

  newConfig.methods.map((method) => {
    if(method.type === 'list') {
      if(!('filters' in method.config)) {
        method.config.filters = []
      }

      return method
    }
  });

  return newConfig;
};
