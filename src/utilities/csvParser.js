export const concatCSVObject = (rows) => {
  const string = rows.map(
    (record) => record.map(
      (str) => (String(str).match(/(\n|,|")/g) ? `"${ str }"` : str),
    ).join(','),
  ).join('\n');

  return unescape(encodeURIComponent(string));
};

export default (string, delimiter = ',') => {
  const objPattern = new RegExp(
      (
        // Delimiters.
        `(\\${ delimiter }|\\r?\\n|\\r|^)`

        // Quoted fields.
        + '(?:"([^"]*(?:""[^"]*)*)"|'

        // Standard fields.
        + `([^"\\${ delimiter }\\r\\n]*))`
      ),
      'gi',
    );


  const arrData = [[]];

  let arrMatches;

  // eslint-disable-next-line no-cond-assign
  while (arrMatches = objPattern.exec(string.trim())) {
    const strMatchedDelimiter = arrMatches[1];

    if (
        strMatchedDelimiter.length
        && strMatchedDelimiter !== delimiter
      ) {
      arrData.push([]);
    }

    let strMatchedValue;

    if (arrMatches[2]) {
      strMatchedValue = arrMatches[2].replace(
          new RegExp('""', 'g'),
          '"',
        );
    } else {
      // eslint-disable-next-line prefer-destructuring
      strMatchedValue = arrMatches[3];
    }

    arrData[arrData.length - 1].push(strMatchedValue);
  }

  const columns = arrData.shift().map((item) => item.trim());

  return {
    columns,
    data: arrData.map((item) => {
      const computedItem = {};
      columns.forEach((column, index) => {
        computedItem[column] = item[index].trim();
      });
      return computedItem;
    }),
  };
};
