import defaultCollectionConfig from './defaults/collection';
import defaultCollectionColumn, {
  availableTypes as availableCollectionColumnTypes,
  config as defaultCollectionColumnConfig,
} from './defaults/collectionColumn';
import defaultCollectionEditor from './defaults/collectionEditor';
import defaultCollectionEditorField, {
  availableTypes as availableCollectionEditorFieldTypes,
  config as defaultCollectionEditorFieldConfig,
} from './defaults/collectionField';
import defaultMenuItem from './defaults/menuItem';
import defaultCollectionAction from './defaults/collectionAction';
import defaultCollectionActionField, {
  config as defaultCollectionActionFieldConfig,
} from './defaults/collectionActionField';
import migrate from './migrations/migrate';

import defaultCollectionFilter from './defaults/collectionFilter';

export {
  defaultCollectionConfig,
  defaultCollectionColumn,
  availableCollectionColumnTypes,
  defaultCollectionColumnConfig,
  defaultCollectionEditor,
  defaultCollectionEditorField,
  defaultMenuItem,
  availableCollectionEditorFieldTypes,
  defaultCollectionEditorFieldConfig,
  defaultCollectionAction,
  defaultCollectionActionField,
  defaultCollectionActionFieldConfig,
  defaultCollectionFilter,
};

export default (config) => migrate(config);
