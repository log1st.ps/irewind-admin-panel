import commonField from './commonField';

export const availableTypes = [
  'text',
  'number',
  'checkbox',
  'dropdown',
  'collection',
  'datetime',
  // 'children',
].map((value) => ({
  value,
  label: value,
}));

export const config = (type) => ({
  field: null,
  ...(commonField(type) || {}),
});

export default {
  label: null,
  width: null,
  type: null,
  config: null,
};
