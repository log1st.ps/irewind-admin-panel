import { VERSION_METHODS } from '../migrations/versionsConstants';

export const collectionListTypeConfig = {
  endpoint: null,
  editors: [],
  columns: [],
  filters: [],
  defaultSortColumn: null,
  defaultSortDirection: null,
  isEditable: false,
  isRemovable: false,
};

export const collectionActionTypeConfig = {
  method: null,
  endpoint: null,
  fields: [],
  buttonText: 'Submit',
  useConfirmation: false,
  confirmationText: 'Are you sure want to execute this action?',
};

export const collectionMethod = {
  name: null,
  description: null,
  type: null,
  config: {

  },
};

export default {
  methods: [],
  name: null,
  description: null,
  version: VERSION_METHODS,
};
