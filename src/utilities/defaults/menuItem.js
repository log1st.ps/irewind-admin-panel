export default {
  collection_id: null,
  order_position: 0,
  sref: null,
  parent_id: null,
  collapsed: 1,
  menu_name: null,
  icon: null,
  id: null,
  url: null,
};
