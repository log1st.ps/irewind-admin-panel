export const availableTypes = [
  'text',
  'number',
  'boolean',
  'datetime',
  'json',
  'pre',
  'url',
].map((value) => ({
  value,
  label: value,
}));

export const config = (type) => ({
  text: {
    maxLength: 0,
  },
  number: {
    type: 'integer',
    thousandDelimiter: ' ',
    decimalDelimiter: '.',
    decimalLength: 2,
  },
  boolean: {
    useCheckbox: true,
    trueLabel: 'Yes',
    falseLabel: 'No',
  },
  datetime: {
    format: 'YYYY-MM-DD',
  },
  json: {
    pretty: false,
  },
  pre: {

  },
  url: {
    openInNewTab: false,
  },
}[type] || {});

export default {
  field: null,
  label: null,
  width: null,
  widthUnit: 'px',
  type: null,
  config: {

  },
};
