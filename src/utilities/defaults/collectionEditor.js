export default {
  title: null,
  width: null,
  color: null,
  useInEditor: false,
  useAsInline: false,
  fields: [],
};
