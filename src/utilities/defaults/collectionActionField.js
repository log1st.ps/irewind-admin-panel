import collectionField from './collectionField';
import commonField from './commonField';

export const config = commonField;

export default {
  ...collectionField,
  config: config(null),
};
