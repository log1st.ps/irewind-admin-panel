export default {
  name: null,
  method: null,
  endpoint: null,
  description: null,
  buttonText: 'Submit',
  useConfirmation: false,
  confirmationText: 'Are you sure want to execute this action?',
  fields: [],
};
