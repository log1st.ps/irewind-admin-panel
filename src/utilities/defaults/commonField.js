export default (type) => ({
  text: {
    rows: 1,
  },
  number: {
    min: 0,
    max: null,
    step: 1,
  },
  checkbox: {
    type: 'checkbox',
  },
  dropdown: {
    options: [],
    isMultiple: false,
  },
  collection: {
    collection: null,
    keyField: null,
    labelField: null,
    isMultiple: false,
    fieldType: 'dropdown',
  },
  children: {
    collection: null,
    editor: null,
    editorWidth: 50,
    tableWidth: 50,
    editorColor: null,
    tableColor: null,
    keys: {},
  },
}[type] || {});
