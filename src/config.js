const l = window.location;

export const facebookAppId = '323445897713125';
export const googleAppId = '908335193411-h1atsq9jlo5lmu7sq7krtdnmiadgn3gu.apps.googleusercontent.com';
export const googleMapsId = 'AIzaSyDPdlvsX9rqEGxI3J6tqU9MLAK_NtJN0yc';
export const apiBaseUrl = 'https://irewind.com/vps-api';
export const apiPublicUrl = 'https://irewind.com/public-api/v1';
export const authRedirectUrl = `${ l.protocol }//${ l.hostname }/admin/`;
