
export default [
  {
    name: 'home',
    path: '/',
    component: () => import('components/layout/layout'),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: ':idLocation(\\d+)',
        component: () => import('components/location/location'),
        children: [
          {
            name: 'overview',
            path: '',
            component: () => import('components/overview/overview'),
          },
          {
            name: 'channel',
            path: 'channel',
            component: () => import('components/channel/channel'),
          },
          {
            name: 'upload',
            path: 'upload',
            component: () => import('components/upload/upload'),
          },
          {
            name: 'uplmeta',
            path: 'uplmeta',
            component: () => import('components/upload-metadata/upload-metadata'),
          },
          {
            name: 'publish',
            path: 'publish',
            component: () => import('components/publish/publish'),
          },
          {
            name: 'videos',
            path: 'videos',
            component: () => import('components/videos/videos'),
          },
          {
            name: 'analytics',
            path: 'analytics',
            component: () => import('components/analytics/analytics'),
          },
          {
            name: 'reporting',
            path: 'reporting',
            component: () => import('../components/reporting/layout'),
            children: [
              {
                name: 'configurator',
                path: 'configurator',
                component: () => import('../components/configurator/configurator'),
                children: [
                  {
                    name: 'reporting-configurator-editor',
                    path: ':editor',
                    component: () => import('../components/configurator/forms/editor'),
                  },
                ],
              },
              {
                name: 'user-timing-import',
                path: 'user-timing',
                component: () => import('../components/user-timing/user-timing'),
              },
              {
                name: 'camera-locator',
                path: 'camera-locator',
                component: () => import('../components/camera-locator/camera-locator'),
              },
              {
                name: 'reporting-report',
                path: ':item?',
                component: () => import('../components/reporting/reporting'),
              },
            ],
          },
        ],
      },
    ],
  },
  { // Always leave this as last one
    name: '404',
    path: '*',
    component: () => import('components/404/404'),
  },
];
