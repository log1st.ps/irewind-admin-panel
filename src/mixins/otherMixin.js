export default {
  methods: {
    getLocalIp() {
      return new Promise((resolve) => {
        const RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

        if (RTCPeerConnection) {
          const rtc = new RTCPeerConnection({ iceServers: [] });
          if (1 || window.mozRTCPeerConnection) {
            rtc.createDataChannel('', { reliable: false });
          }

          rtc.onicecandidate = (evt) => {
            // eslint-disable-next-line no-use-before-define
            if (evt.candidate) grepSDP(`a=${ evt.candidate.candidate }`);
          };
          rtc.createOffer((offerDesc) => {
            // eslint-disable-next-line no-use-before-define
            grepSDP(offerDesc.sdp);
            rtc.setLocalDescription(offerDesc);
          }, (e) => { console.warn('offer failed', e); });


          const addrs = Object.create(null);
          addrs['0.0.0.0'] = false;
          const updateDisplay = (newAddr) => {
            if (newAddr in addrs) return;
            addrs[newAddr] = true;
            const displayAddrs = Object.keys(addrs).filter((k) => addrs[k]);
            resolve(displayAddrs);
          };

          const grepSDP = (sdp) => {
            sdp.split('\r\n').forEach((line) => {
              // eslint-disable-next-line no-bitwise
              if (~line.indexOf('a=candidate')) {
                const parts = line.split(' ');

                const addr = parts[4];


                const type = parts[7];
                if (type === 'host') updateDisplay(addr);
                // eslint-disable-next-line no-bitwise
              } else if (~line.indexOf('c=')) {
                const parts = line.split(' ');


                const addr = parts[2];
                updateDisplay(addr);
              }
            });
          };
        }
      });
    },
  },
};
