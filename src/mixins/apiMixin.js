import axios from 'axios';
import qs from 'qs';
import { buildAxiosConfig, getApiUrl } from '../../worker/common';

export default {
  methods: {
    sendSWMessage(message, params = {}) {
      return new Promise((resolve) => {
        const channel = new MessageChannel();
        channel.port1.onmessage = (e) => {
          resolve(e.data);
        };

        const { token } = this.$store.state.auth.credentials;

        this.$w.postMessage(
          {
            command: message,
            params,
            token,
          },
          [channel.port2],
        );
      });
    },
    computeUsersInTiming({
                           users,
                           columns,
                           attachedColumns,
     }) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('computeUsersInTiming', {
          users,
          columns,
          attachedColumns,
        })
          .then((res) => {
            resolve(res);
          })
          .catch((res) => {
            reject(res);
          });
      });
    },
    computeTimingsInTiming({
                             users,
                             attachedColumns,
                             timestampColumns,
                             columnCameras,
                             timeZone,
                             date,
                             isTimeZoneDetected,
      filters,
     }) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('computeTimingsInTiming', {
          users,
          attachedColumns,
          timestampColumns,
          columnCameras,
          timeZone,
          date,
          isTimeZoneDetected,
          filters,
        })
          .then((res) => {
            resolve(res);
          })
          .catch((res) => {
            reject(res);
          });
      });
    },
    formatDataResponse({
      records,
      columns,
     }) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('formatDataResponse', {
          records,
          columns,
        })
          .then((res) => {
            resolve(res);
          })
          .catch(reject);
      });
    },
    apiGet(url, config) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('get', {
          url,
          config,
        })
          .then((res) => {
            if (res.status === 401) {
              reject(res);
            } else {
              resolve(res);
            }
          })
          .catch(reject);
      });
    },
    apiPost(url, data, config, isFormData = false) {
      return new Promise((resolve, reject) => {
        if (isFormData) {
          const { token } = this.$store.state.auth.credentials;

          axios.post(
            getApiUrl(url),
            isFormData ? data : qs.stringify(data),
            buildAxiosConfig({
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
              ...config,
            }, token),
          )
            .then(({ data: response }) => {
              resolve(response);
            })
            .catch(reject);
        } else {
          this.sendSWMessage('post', {
            url,
            data,
            config,
          })
            .then((res) => {
              if (res.status === 401) {
                reject(res);
              } else {
                resolve(res);
              }
            })
            .catch(reject);
        }
      });
    },
    apiPut(url, data, config) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('put', {
          url,
          data,
          config,
        })
          .then((res) => {
            if (res.status === 401) {
              reject();
            } else {
              resolve(res);
            }
          })
          .catch(reject);
      });
    },
    apiDelete(url, data, config) {
      return new Promise((resolve, reject) => {
        this.sendSWMessage('delete', {
          url,
          data,
          config,
        })
          .then((res) => {
            if (res.status === 401) {
              reject();
            } else {
              resolve(res);
            }
          })
          .catch(reject);
      });
    },
  },
};
