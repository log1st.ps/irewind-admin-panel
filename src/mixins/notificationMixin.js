export default {
  methods: {
    notify({
      title,
      message,
      color,
    }) {
      return new Promise((onDismiss) => {
        this.$q.notify({
          message: title,
          detail: message,
          color,
          onDismiss,
          actions: [
            {
              icon: 'close',
              'v-close-overlay': true,
            },
          ],
        });
      });
    },
    notifyError({
      title,
      message,
    }) {
      return this.notify({
        title: title || 'Error',
        message,
        color: 'red',
      });
    },
    notifyInfo({
      title,
      message,
    }) {
      return this.notify({
        title: title || 'Info',
        message,
        color: 'green',
      });
    },
  },
};
