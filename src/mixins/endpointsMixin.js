import apiMixin from './apiMixin';
import migrateCollectionConfig from '../utilities/migrateConfig';
import notificationMixin from './notificationMixin';

export default {
  mixins: [apiMixin, notificationMixin],
  methods: {
    exportRowsInCSV(columns, data) {
      const rows = [columns];

      data.forEach((record) => {
        const row = [];
        columns.forEach((column) => {
          row.push(record[column]);
        });
        rows.push(row);
      });

      const string = rows.map((row) => row.join(',')).join('\n');

      const a = document.createElement('a');
      a.download = 'download.csv';
      a.href = `data:text/csv;base64,${ btoa(unescape(encodeURIComponent(string))) }`;

      a.click();
    },
    deleteRecord({ endpoint, record }) {
      return new Promise((resolve, reject) => {
        this.apiDelete(
          `${ (endpoint.match(/(.*)\/list/) || [])[1] }/delete`,
          record,
        )
          .then(({ Status, Message, message }) => {
            if (Status === 'Ok') {
              resolve();
            } else {
              reject();
              this.notifyError({
                title: 'Unable to delete record',
                message: Message || message,
              });
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to delete record',
            });
            reject();
          });
      });
    },
    fetchData({
      endpoint,
      query,
    }) {
      return new Promise((resolve, reject) => {
        const params = {
          ...query,
        };

        this.apiGet(endpoint, {
          params,
        })
          .then(({
            Status, Message, results,
          }) => {
            if (Status === 'Ok') {
              resolve({
                records: results,
                count: results.length,
              });
            } else {
              reject();
              this.notifyError({
                title: 'Unable to fetch data',
                message: Message,
              });
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to fetch data',
            });
            reject();
          });
      });
    },
    fetchCollectionByMenuId(id) {
      return new Promise((resolve, reject) => {
        this.apiGet('rest-sql/menus/get', {
          params: {
            id,
          },
        })
          .then(({ Status, Message, results }) => {
            if (Status === 'Ok' && results && results.length) {
              const { collection_id: collectionId } = results[0];

              if (collectionId) {
                this.fetchCollection(collectionId)
                  .then((res) => {
                    resolve(res);
                  });
              } else {
                this.notifyError({
                  title: 'Unable to load collection',
                  message: 'No specified collection for menu item',
                });
              }
            } else {
              reject();
              this.notifyError({
                title: 'Unable to load collection',
                message: Message || 'Not found',
              });
            }
          })
          .catch(() => {
            reject();
          });
      });
    },
    saveCollection(record) {
      return new Promise((resolve, reject) => {
        this.apiPost('rest-sql/collections/set', record)
          .then(({ Status, Message }) => {
            if (Status === 'Ok') {
              resolve();
              this.notifyInfo({
                title: `Collection ${ record.collection_id } has been updated`,
              });
            } else {
              reject();
              this.notifyError({
                title: 'Unable to save collection',
                message: Message,
              });
            }
          })
          .catch(() => {
            reject();
            this.notifyError({
              title: 'Unable to save collection',
            });
          });
      });
    },
    fetchEndpointFields(endpoint) {
      return new Promise((resolve, reject) => {
        this.apiGet(endpoint)
          .then(({ Status, Message, results }) => {
            if (Status === 'Ok') {
              resolve(results);
            } else {
              this.notifyError({
                title: 'Unable to load endpoint fields',
                message: Message,
              });
              reject();
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to load endpoint fields',
            });
            reject();
          });
      });
    },
    removeMenuItem(record) {
      return new Promise((resolve, reject) => {
        this.apiDelete('rest-sql/menus/remove', record)
          .then(({ Status, Message }) => {
            if (Status === 'Ok') {
              resolve();
            } else {
              reject();
              this.notifyError({
                title: 'Unable to remove menu, item',
                message: Message,
              });
            }
          })
          .catch(() => {
            reject();
            this.notifyError({
              title: 'Unable to remove menu item',
            });
          });
      });
    },
    saveMenuItem(record, notify = true) {
      return new Promise((resolve, reject) => {
        this.apiPost('rest-sql/menus/set', record)
          .then(({
            Status, Message, message,
          }) => {
            if (Status === 'Ok') {
              resolve();
              if (notify) {
                this.notifyInfo({
                  title: `Menu item ${ record.menu_name } has been ${ record.id ? 'saved' : 'created' }`,
                });
              }
            } else if (notify) {
              this.notifyError({
                title: `Unable to ${ record.id ? 'save' : 'create' } menu item`,
                message: Message || message,
              });
            }
            resolve();
          })
          .catch(() => {
            if (notify) {
              this.notifyError({
                title: `Unable to ${ record.id ? 'save' : 'create' } menu item`,
              });
            }
            reject();
          });
      });
    },
    fetchMenuItems() {
      return new Promise((resolve, reject) => {
        this.apiGet('rest-sql/menus/list')
          .then(({
                   Status, Message, message, results,
                 }) => {
            if (Status !== 'Ok') {
              this.notifyError({
                title: 'Unable to fetch menu',
                message: Message || message,
              });
              reject();
            } else {
              resolve(results);
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to fetch menu',
            });
            reject();
          });
      });
    },
    fetchCollection(id, migrate = true) {
      return new Promise((resolve, reject) => {
        this.apiGet('rest-sql/collections/get', {
          params: {
            collection_id: id,
          },
        })
          .then(({
            Status, Message, message, results,
          }) => {
            if (Status === 'Ok') {
              resolve(migrate ? {
                ...migrateCollectionConfig(JSON.parse(results[0].collection_config) || {}),
              } : JSON.parse(results[0].collection_config) || {});
            } else {
              reject();
              this.notifyError({
                message: Message || message,
              });
            }
          })
          .catch(() => {
            reject();
          });
      });
    },
    fetchCollections() {
      return new Promise((resolve, reject) => {
        this.apiGet('rest-sql/collections/list')
          .then(({
            Status, Message, message, results,
          }) => {
            if (Status === 'Ok') {
              resolve(results);
            } else {
              this.notifyError({
                title: 'Unable to load collections',
                message: Message || message,
              });
              reject();
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to load collections',
            });
            reject();
          });
      });
    },
    fetchCameraSegments({
      locationId,
      camId,
      timestampStart,
      timestampEnd,
    }) {
      // eslint-disable-next-line
      return new Promise((resolve, reject) => {
        this.apiGet('/rest-sql/cameras/getSegments', {
          params: {
            cam_id: camId,
            location_id: locationId,
            timestamp_start: timestampStart,
            timestamp_end: timestampEnd,
          },
        })
          .then(({
            Status,
            results,
            Message,
            message,
           }) => {
            if (Status === 'Ok') {
              resolve(results);
            } else {
              this.notifyError({
                title: 'Unable to load camera segments',
                message: Message || message,
              });
              reject();
            }
          })
          .catch(() => {
            this.notifyError({
              title: 'Unable to load camera segments',
            });
            reject();
          });
      });
    },
  },
};
